package com.globus.erpar.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.model.GmARRejectPOEmailModel;
import com.globus.erpar.service.GmARCustPOService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;


/**
 * This GmARCustPOController class used to load/save the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */

@RequestMapping("/api")
@RestController
public class GmARCustPOController {

	@Autowired
	GmARCustPOService gmARCustPOService;
	
	/**
	 * This method is used to load the PO report details
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	
	@RequestMapping(value = "LoadPOreport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARCustPOModel> LoadpendingPO(@RequestBody GmARCustPOModel gmARCustPOModel) throws Exception {
		return gmARCustPOService.fetchPendingPODetails(gmARCustPOModel);
	}
	/**
	 * This method is used to fetch the Order and PO Details for selected Order
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	
